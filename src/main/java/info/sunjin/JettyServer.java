package info.sunjin;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import info.sunjin.servlet.HelloServlet;

public class JettyServer {
    private Server server;

    public void start() throws Exception {
        server = new Server(threadPool());
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8099);
        server.setConnectors(new Connector[] { connector });

        // router
        ServletHandler servletHandler = new ServletHandler();
        servletHandler.addServletWithMapping(BlockingServlet.class, "/status");
        servletHandler.addServletWithMapping(AsyncServlet.class, "/heavy/async");
        servletHandler.addServletWithMapping(HelloServlet.class, "/hello");
        server.setHandler(servletHandler);

        // start
        server.start();
    }

    /**
     * thread config
     * 
     * @return QueuedThreadPool
     */
    private QueuedThreadPool threadPool() {
        int maxThreads = 100;
        int minThreads = 10;
        int idleTimeout = 120;
        return new QueuedThreadPool(maxThreads, minThreads, idleTimeout);
    }

}