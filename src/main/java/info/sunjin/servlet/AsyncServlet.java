package info.sunjin.servlet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * 簡単な奴を実装したい...
 */
abstract public class AsyncServlet extends HttpServlet {

    private Gson gson = new Gson();

    /**
     * Serial
     */
    private static final long serialVersionUID = -6633332824989339638L;

    /**
     * ビジネスロジック
     */
    abstract protected Object mainLogic(HttpServletRequest request);

    /**
     * ObjectをJsonに変換
     * 
     * @param result
     * @return
     */
    private String resultJson(Object result) {
        return gson.toJson(result);
    }

    /**
     * GET
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AsyncContext async = request.startAsync();
        ServletOutputStream out = response.getOutputStream();

        // logic
        Object result = mainLogic(request);
        String resultJson = resultJson(result);

        ByteBuffer content = ByteBuffer.wrap(resultJson.getBytes(StandardCharsets.UTF_8));

        WriteListener writeListener = new WriteListener() {

            @Override
            public void onWritePossible() throws IOException {
                while (out.isReady()) {
                    if (!content.hasRemaining()) {
                        response.setStatus(HttpServletResponse.SC_OK);
                        async.complete();
                        return;
                    }
                    out.write(content.get());
                }

            }

            @Override
            public void onError(Throwable t) {
                getServletContext().log("Async Error", t);
                async.complete();
            }
        };

        out.setWriteListener(writeListener);

    }

}