package info.sunjin.servlet;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class HelloServlet extends AsyncServlet {

    /**
     * Serial
     */
    private static final long serialVersionUID = 4313399223623495539L;

    protected Object mainLogic(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", "ok");
        return resultMap;
    }
}